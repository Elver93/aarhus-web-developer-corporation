﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web.Mvc;
using AarhusWebDevCoop.ViewModels;
using System.Net.Mail;
using Umbraco.Core.Models;

namespace AarhusWebDevCoop.Controllers
{
    public class ContactFormSurfaceController : SurfaceController
    {
        // GET: ContactFormSurface
        public ActionResult Index()
        {
            // returnerer partialview contactfrom.cshtml
            return PartialView("ContactForm", new ContactForm());
        }

        // denne method bliver kørt når du trykker på submit button i form 
        public ActionResult HandleFormSubmit(ContactForm model)
        {
            // tjekker om contactfrom er valid
            if (!ModelState.IsValid)
            {
                return CurrentUmbracoPage();
            }

            // standard smtp mail function 
            MailMessage message = new MailMessage();
            message.To.Add("elvermikkel9@gmail.com");
            message.Subject = model.Subject;
            message.From = new MailAddress(model.Email, model.Name);
            message.Body = model.Message;

            using (SmtpClient smtp = new SmtpClient())
            {
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.UseDefaultCredentials = false;
                smtp.EnableSsl = true;
                smtp.Host = "smtp.gmail.com";
                smtp.Port = 587;
                smtp.Credentials = new System.Net.NetworkCredential("elvermikkel9@gmail.com", "luoerhhuklyzgtvv");
                // send mail   
                smtp.Send(message);
            }

            //sætter min variable tempdata til true fra standard er den null
            TempData["success"] = true;

            // bruger content service api til gemme inputfelter ned i parameter
            IContent msg = Services.ContentService.CreateContent(model.Subject, CurrentPage.Id, "message");
            msg.SetValue("messageName",model.Name);
            msg.SetValue("email",model.Email);
            msg.SetValue("subject",model.Subject);
            msg.SetValue("messageContent",model.Message);

            // gemmer besked ned i database så den kan ses i backoffice
            Services.ContentService.Save(msg);

            return RedirectToCurrentUmbracoPage();
        }
    }
}